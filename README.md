# DropdownCSS

> Create easy and quick dropdowns using only HTML and CSS. No JavaScript!

## Getting started

Download `dropdown.css` and link to it in the HTML file where you want to use dropdowns.

```html
<link rel="stylesheet" href="css/dropdown.css" type="text/css"/>
```

## Add a dropdown 

To add a dropdown, use the following code

```html
<div class="dropdown-container">
    <input type="checkbox" id="myDropdown"/>
    <label for="myDropdown">
        <h2>Click me</h2>
    </label>
    
    <div class="dropdown-content">
        <h3>Boo!</h3>
        <p>Now you can see me! Click again to hide me.</p>
    </div>
</div>
```

### Breakdown of the code

1. First, we create a container for our dropdown

`<div class="dropdown-container> ... </div>`

2. Secondly, we add a checkbox or radio input. This input will be hidden.

`<input type="checkbox" id="myDropdown"/>`

3. We assign a label for this input, when the label is clicked, the input will be triggered. The input on its turn will trigger whether or not to show the dropdown.

`<label for="myDropdown"> ... </label>`

4. Finally, we create a dropdown-content element where the actual content of the dropdown will be in. You can put anything you want in it! Cool right?

`<div class="dropdown-content"> ... </div>`

## Different types of dropdown

There are two different dropdown types. 

1. 'checkbox' dropdown
    Dropdowns with 'checkbox' inputs will remain open, even after other 'checkbox' dropdowns are opened.
    `<input type='checkbox' id='myCheckboxDropdown'/>`
    
2. 'radio' dropdown
    These dropdowns will remain open as long as you do not open another 'radio' type dropdown. 
    By default, this will close ***all*** 'radio' dropdowns on the page. To create a group of 'radio' dropdowns, 
    assign the inputs with the same 'name' attribute. For example:
    
    ```html
    <input type='radio' id='myDropdown1' name='grouped'/> 
    <input type='radio' id='myDropdown2' name='grouped'/>
    <input type='radio' id='myDropdown3' name='grouped'/>
    ...
    ```